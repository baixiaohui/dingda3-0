require(["jquery","eap","mustache","bike","utils","err"],function($,eap,mustache,bike,utils,err) {
	var RecordList = $("#RecordList");
	var RecordListTemplate = RecordList.html();
	var hirestatus = {
		"1":"已租",
		"2":"已还",
		"5":"异常租车",
		"6":"异常还车"
	};
	//加载数据
	bike.queryRecords()
	.then(function(res){
		eap.each(res.data,function(data){
			data.hirestatus = hirestatus[data.hirestatus]
			data.useTime = utils.formatElapsedDate(data.renttime,data.restoretime);
		})
		return res;
	})
	.done(function(res){
		if(res.retcode == err.USER_NOT_LOGIN.code){
			window.location.href = "login.html";
			return;
		}
		RecordList.html(mustache.render(RecordListTemplate, {
			data : res.data
		}));
		RecordList.show();
		$("#record-header").show();
		if(res.data.length >10){
			$(".foot-btn").show();
		}
	})
	.fail(function(){
		utils.msg("租车记录查询失败...")
	})
	.always(utils.hideLoading);

	var touchEvents = {
        touchstart: "touchstart",
        touchmove: "touchmove",
        touchend: "touchend",

        /**
         * @desc:判断是否pc设备，若是pc，需要更改touch事件为鼠标事件，否则默认触摸事件
         */
        initTouchEvents: function () {
            if (isPC()) {
                this.touchstart = "mousedown";
                this.touchmove = "mousemove";
                this.touchend = "mouseup";
            }
        }
    };
    // $(document).bind(touchEvents.touchstart, function (event) {
    //         event.preventDefault();
            
    //     });
    //     $(document).bind(touchEvents.touchmove, function (event) {
    //         event.preventDefault();
            
    //     });

    //     $(document).bind(touchEvents.touchend, function (event) {
    //         event.preventDefault();
            
    //     });


    //     $("body").on("touchstart", function(e) {
				// 	// e.preventDefault();
				// 	 startX = e.originalEvent.changedTouches[0].pageX,
				// 	 startY = e.originalEvent.changedTouches[0].pageY;
				// 	});
				// 	$("body").on("touchmove", function(e) {
				// 		//e.preventDefault();
				// 		 moveEndX = e.originalEvent.changedTouches[0].pageX,
				// 		 moveEndY = e.originalEvent.changedTouches[0].pageY,
				// 		 X = moveEndX - startX,
				// 		 Y = moveEndY - startY;
				// 	 // if ( Math.abs(X) > Math.abs(Y) && X > 0 ) {
				// 	 //  //alert("left 2 right");
				// 	 // }
				// 	 // else if ( Math.abs(X) > Math.abs(Y) && X < 0 ) {
				// 	 //  //alert("right 2 left");
				// 	 // }
				// 	 // else
				// 	  if ( Math.abs(Y) > Math.abs(X) && Y > 0) {
				// 	 // alert("top 2 bottom");
    //           $(".air-atop").slideDown();
				// 	 }
				// 	 else if ( Math.abs(Y) > Math.abs(X) && Y < 0 ) {
				// 	  //alert("bottom 2 top");
				// 	    $(".air-atop").slideUp();
				// 	 }
				// 	/* else{
				// 	  alert("just touch");
				// 	 }*/
				// });
});