require(["eap","md5","utils","err"],function(eap,md5,utils,err) {
	 var cliwidth=$(window).width();
	 var cliheight=$(window).height();
	 var result=true;
	 var view={
		  getCode:{
		    getSelect:function(){
		      return '#getCode';
		    },
		  },
	    tel:{
		    getSelect:function(){
		      return '.tel';
		    },
		  },
		   code:{
		    getSelect:function(){
		      return '.code';
		    },
		  },
		  tip:{
		    getSelect:function(){
		      return '.tip';
		    },
		  },
		   input:{
		    getSelect:function(){
		      return '.input';
		    },
		  },
		  zhuCe:{
		  	getSelect:function(){
		  		return '#zhuCe';
		  	},
		  },
	 };
	 var show={
	 	isPhone:function(phone) {
        var re=/^1(3|4|5|7|8)\d{9}$/;
        if (re.test(phone)){
            return true;
        }else{
        	  $(view.tip.getSelect()).css("display","block").html("手机号码输入有误");
            return false;
         }
     },
     check:function(){
     	var telReg = /^(13[0-9]|14[0-9]|15[0-9]|17[0-9]|18[0-9])\d{8}$/i;
     	var checkReg = /^\d{6}$/i;
        $(view.input.getSelect()).off("blur;").on("blur",function(){
          var val=$(this).val();
          result=true;
          if(!val){
          	result=false;
          	 $(view.tip.getSelect()).html("不能为空").show();

          	return result;
          }else{
          	 if($(this).hasClass('tel')){
	               if(!telReg.test(val)){
	                  $(view.tip.getSelect()).html('手机号码输入有误').show();
	                    result=false;
	                    return result;
	               }else{
	               	   result=true;
	                   return result;
	               }
              }else if($(this).hasClass('code')){
              	if(!checkReg.test(val)){
              		 $(view.tip.getSelect()).html('验证码错误').show();
              		   result=false;
	                   return result;
              	}else{
              		 result=true;
	                 return result;
              	}
              }
          }
        });
        $(view.input.getSelect()).off("focus").on("focus",function(){
        	$(view.tip.getSelect()).html("").hide();
        })
     },
	   guideBoxInit:function(){
		 var box=$(view.guideRoad.getSelect()).empty();
         box.append(div1).show();
		 }
	 };
	 var ctrl={
			init:function(){
				show.check();
				//show.guideBoxInit();
		    var itemFunc = null;
		    for (var event in ctrl.bindEvent) {
		        itemFunc = ctrl.bindEvent[event]
		        if (itemFunc && typeof (itemFunc) === "function") {
		            itemFunc();
		        };
		    };
			},
			bindEvent:{
				//获取验证码
			   'getCodeEvent':function(){
				   $(view.getCode.getSelect()).unbind("click").bind("click",function(){
				   	   var telephone=$(view.tel.getSelect()).val();
				   	   if(!telephone){
				   	   	   $(view.tip.getSelect()).css("display","block").html("手机号码不能为空");
				   	   }else{
				   	   	//show.check($(view.tel.getSelect()));
				   	   	  if(!show.isPhone(telephone)){
                      return false;
                        $(view.tel.getSelect()).off("focus").on("focus",function(){
								        	$(view.tip.getSelect()).html("").hide();
								        })
				   	   	  }else{

				           var time=6;
				           var timer = setInterval(function(){
			              if(time==0){
			              	time=6;
			              	$(view.getCode.getSelect()).text("重新获取")
			              	$(view.getCode.getSelect()).removeAttr("disabled");
			              	clearInterval(timer);
			              }else{
			                $(view.getCode.getSelect()).text(time + "s后重发").css("color","#666");
			                time --;
			              	$(view.getCode.getSelect()).attr("disabled","true");
			              }
				           },500);

				   	   	  }
				   	    }
				     }); 
				    },
				    "zhuceEvent":function(){  //注册
               $(view.zhuCe.getSelect()).unbind("click").bind("click",function(){
                  
                  if(result){
                  	window.location.href="index2.html"; 
                  }
               });
				    }
			   }
		 
	};
	 ctrl.init();
});

