require(["eap","md5","utils","err"],function(eap,md5,utils,err) {
	 var cliwidth=$(window).width();
	 var cliheight=$(window).height();
	 var imgArray1=["image/icon_bike0@2x.png","image/icon_bike1@2x.png","image/icon_bike1@2x.png"];
	 var pArray1=["助力车","无桩车","有桩车"];
	 var imgArray2=["image/icon_bike0@2x.png","image/icon_bike1@2x.png","image/icon_bike1@2x.png"];
	 var pArray2=["帮助指南","用户手册","问题反馈"];
	 var view={
		  allmap:{
		    getSelect:function(){
		      return '#allmap';
		    },
		  },
	    swipeCode:{
	    getSelect:function(){
	      return '#swipeCode';
	    },
	  },
	  bikeType:{
	  	getSelect:function(){
	  		return '#bikeType'
	  	},
	  },
	  bikeTypeBox:{
	  	getSelect:function(){
	  		return '.bikeTypeBox'
	  	},
	  },
	  pageone:{
	  	getSelect:function(){
	  		return '#pageone'
	  	},
	  },
	  reportBtn:{
	  	getSelect:function(){
	  		return '#reportBtn'
	  	},
	  },
	  'returnBtn':{
	  	getSelect:function(){
	  		return '#returnBtn'
	  	},
	  },
	  'moni':{
	  	getSelect:function(){
        return '#moni'
	  	},
	  },
	  'moniBox':{
	  	getSelect:function(){
	  		return '#moniBox';
	  	},
	  },
	 };
	 var model={
	 	mapInit:function(){
		      $(view.allmap.getSelect()).width(cliwidth);
		      $(view.allmap.getSelect()).height(cliheight);
		      var swipeWidth=$(view.swipeCode.getSelect()).width();
			 		 var leftdis=(cliwidth-swipeWidth)/2 ;
			 		 //var leftdis=(cliwidth - 87)/2;
		       console.log(leftdis);
		       $(view.swipeCode.getSelect()).css('left',leftdis);
			 	},
	 };
	 var show={
	 	bikeBoxInit:function(box,imgArray,pArray){
		 //	$(view.bikeTypeBox.getSelect()).empty();
			 var ul=$("<ul>");
		   for(var i=0;i<3;i++){
		   	 var li=$("<li>");
			    li.append($("<img>").attr("src",imgArray[i]));
			    li.append($("<p>").text(pArray[i])); 

	        ul.append(li).append(li).append(li);
			 }
	      box.append(ul);
	   },
	   guideBoxInit:function(){
		 	 var box=$(view.moniBox.getSelect()).empty();
			 	 var data={
			 	 	"name":"江辉大厦南门",
			 	 	"kilo":38,
			 	 	"canrent":18,
			 	 	"canback":32
			 	 };
			 	 var text=["米数","可借","可还"];
			 	 var h2=$("<h2>");
			 	 h2.addClass("title").text(data.name);
			 	 var ul=$("<ul>");
			 	 ul.addClass("numlist");
			 	 for(var i=0;i<3;i++){
			 	 	var li=$("<li>");
			 	 	li.append($("<p>").addClass("p1").text(data.kilo));
			 	 	li.append($("<p>").addClass("p2").text(text[i]));
		      ul.append(li);
			 	 }
	      var a=$("<a>").addClass("daohang");
	      a.attr("href","daohang.html").text("导航");
	       box.append(h2).append(ul).append(a);

		   }
	 };
	 var ctrl={
			init:function(){
				model.mapInit();
			    var itemFunc = null;
			    for (var event in ctrl.bindEvent) {
			        itemFunc = ctrl.bindEvent[event]
			        if (itemFunc && typeof (itemFunc) === "function") {
			            itemFunc();
			        };
			    };
				},
			bindEvent:{
			 	'bikeEvent':function(){
		      $(view.bikeType.getSelect()).unbind("click").bind("click",function(){
		      	  var box=$(view.bikeTypeBox.getSelect());
		      	  box.empty();
		      	  show.bikeBoxInit(box,imgArray1,pArray1);
			        $(view.bikeTypeBox.getSelect()).slideToggle('slow','swing'); 
			      });
			   },
			  'reportEvent':function(){
		      $(view.reportBtn.getSelect()).unbind("click").bind("click",function(){
		      	  var box=$(view.bikeTypeBox.getSelect());
		      	  box.empty();
		      	  show.bikeBoxInit(box,imgArray2,pArray2);
			        $(view.bikeTypeBox.getSelect()).slideToggle('slow','swing'); 
			      });
			   },
			  'tapWindow':function(){
			      var content = document.querySelector(view.pageone.getSelect());  
						content.addEventListener("touchend", function(){  
					     if($(view.bikeTypeBox.getSelect()).is(":visible")){
					  	 	  $(view.bikeTypeBox.getSelect()).slideToggle('slow','swing'); 
					  	 }
						});  
			   },
         'moni':function(){
            $(view.moni.getSelect()).unbind("click").bind("click",function(){
               show.guideBoxInit();
               $(view.moniBox.getSelect()).slideToggle('slow','swing');
            });
         },
         'delDaohang':function(){
			      var content = document.querySelector(view.pageone.getSelect());  
						content.addEventListener("touchend", function(){  
			     if($(view.moniBox.getSelect()).is(":visible")){
			  	 	  $(view.moniBox.getSelect()).slideToggle('slow','swing'); 
			  	  }
				 });  
			 },
			 //   'returnEvent':function(){
			 //      	$(view.returnBtn.getSelect()).click(function () {
				//         var speed=200;//滑动的速度

				//         $(window).animate({ scrollTop: 0 }, speed);
				//         return false;
				//     });
			 //   }
			 // }
		 }
		};

	 ctrl.init();
});

