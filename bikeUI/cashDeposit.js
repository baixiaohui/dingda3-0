require(["jquery","eap","mustache","bike","pingpp","err","utils"],function($,eap,mustache,bike,pingpp,err,utils) {
	//查询用户状态显示注销，开通租车
	var rentBikeBtn = $("#rentBikeBtn");
	var bizFlag = true;
	bike.getBizInfo()
	.done(function(res){
		switch(res.bizstatus){
			case err.CARD_NEW.code:
			case err.CARD_LOGOFF.code:
			bizFlag = true;
			rentBikeBtn.html("开通租车")
			rentBikeBtn.removeClass("btn-danger").addClass("btn-success");
			break;
			default:
			bizFlag = false;
			rentBikeBtn.html("注销租车")
			rentBikeBtn.removeClass("btn-success").addClass("btn-danger");
		}
		rentBikeBtn.css("display", "block");
	})
	.fail(function(e){
		utils.msg("获取用户租车状态失败");
	});
	
	rentBikeBtn.click(function(){
		if(bizFlag){
			utils.dialog({
				title : "开通租车业务",
				msg : "您确定要开通租车业务吗?"
			},function(result){
				var orderid;
				utils.setButtonState(rentBikeBtn,true);
				bike.enableTrade()
				.then(function(res){
					if(err.isOk(res.retcode)){
						utils.msg("获取支付数据...");
						orderid = res.orderid;
						return bike.getCharge(orderid)
					} else {
						utils.msg("开通租车业务失败");
					}
				})
				.done(function(res){
					if(err.isOk(res.retcode)){
						utils.msg("调起微信支付...");
						pingpp.createPayment(res.chargedata,function(result, err){
						    if (result == "success") {
						    	bike.checkOrder(orderid)
								.done(function(res){
									utils.notice("开通租车业务成功,快去租车吧！")
									setTimeout(function(){
										location.href = "index.html"
									},2000)
								})
						    } else if (result == "fail") {
						    	utils.msg("支付失败");
						    	setTimeout(function(){
						    		history.back()
								},1000)
						    } else if (result == "cancel") {
						    	utils.msg("用户取消支付");
						    	setTimeout(function(){
						    		history.back()
								},1000)
						    }
						});
					} else {
						utils.msg("获取支付数据失败");
					}
				})
				.always(function(){
					//utils.setButtonState(rentBikeBtn,false);
				});
			});
		} else {
			utils.dialog({
				title : "注销租车业务",
				msg : "您确定要注销租车业务吗?"
			},function(result){
				if(result){
					utils.setButtonState(rentBikeBtn,true);
					bike.disableTrade()
					.done(function(res){
						if(res.bizstatus == err.CARD_LOGOFF.code){
							utils.notice("注销租车业务成功,感谢您的使用！");
							setTimeout(function(){
								location.href = "index.html"
							},2000)
						} else if(res.bizstatus == err.CARD_VALID.code){//如果为可租车状态，后台出错
							utils.msg(res.retmsg);
						} else {
							var retMsg = err.findDescByCode(res.bizstatus);
							if(!eap.isEmpty(retMsg)){
								utils.msg("["+retMsg+"]状态，不能注销");
							} else {
								utils.msg("注销租车业务失败，请稍后再试");
							}
						}
					})
					.fail(function(e){
						utils.msg(e);
					})
					.always(function(){
						utils.setButtonState(rentBikeBtn,false);
					});
				} 
			})
			
		}
	});
});