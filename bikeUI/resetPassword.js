require(["jquery","eap","mustache","bike","utils","err"],function($,eap,mustache,bike,utils,err) {
  		//修改密码
	$("#resetPassWordBtn").click(function(){
		var me = this;
		var userPhone = $("#userPhone").val();
		var checkcode = $("#checkcode").val();
		var password = $("#password").val();
		utils.setButtonState(me,true);
		if(eap.isEmpty(userPhone)){
			utils.msg("请输入手机号");
			utils.setButtonState(me,false);
   		 	return;
		} 
		if(eap.isEmpty(checkcode)){
			utils.msg("请输入验证码");
			utils.setButtonState(me,false);
			return;
		}
		if(eap.isEmpty(password)){
			utils.msg("请输入密码");
			utils.setButtonState(me,false);
			return;
		}
		
		bike.resetpwd(userPhone,password,checkcode)
		.done(function(res){
			if(err.isOk(res.retcode)){
				window.location.href = "login.html";
			} else {
				utils.msg(res.retmsg);
			}
		})
		.always(function(){
			utils.setButtonState(me,false);
		});
	});
	var task;
	var taskConfig = {
		timeCount:30,
	    run: function(){
	    	if(this.timeCount > 0){
	    		$("#checkcodebtn").attr('disabled',true);
	    		$("#checkcodebtn").html(this.timeCount+"s后获取");
	    	} else if(this.timeCount == 0){
	    		eap.TaskMgr.stop(task);
	    	}
	    	this.timeCount--;
	    },
	    onStop : function(){
	    	this.timeCount = 30;
	    	$("#checkcodebtn").attr('disabled',false).html("发送验证码");
	    },
	    interval: 1000 //1 second
	}
	
	//获取注册验证码
	$("#checkcodebtn").click(function(){
		var userPhone = $("#userPhone").val();
		if(eap.isEmpty(userPhone) || userPhone.length != 11){
			utils.msg("手机号码不能为空");
			return;
		}
		if(userPhone.length != 11){
			utils.msg("手机号码长度不正确");
			return;
		}
		task = eap.TaskMgr.start(taskConfig);
		
		bike.getResetPWDVCode(userPhone)
		.done(function(response){
			if(err.isOk(response.retcode)){
				utils.msg(response.retmsg);
			} else {
				utils.msg(response.retmsg);
				eap.TaskMgr.stop(task);
			}
		})
		.fail(function(response){
			utils.msg(response.retmsg);
			eap.TaskMgr.stop(task);
		});
	});
});