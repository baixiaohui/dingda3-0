require(["jquery", "eap", "mustache", "bike", "err", "utils"], function($, eap, mustache, bike, err, utils) {
	//渲染页面
	var siteInfo = eap.urlDecode(window.location.search.substring(1));
	var goRentBikePage = $("#goRentBikePage");
	var rentBikeBtn = $("#rentBikeBtn")
	var siteInfoTemplate = goRentBikePage.html();
	var currentPosDiv;
	//构建数据
	var totalcount = parseInt(siteInfo.totalcount);
	totalcount = totalcount ==0 ? 19 : totalcount;
	siteInfo.poscount = [];
	for(var i = 0 ;i < totalcount;i++){
		var index = getIndex(i+1);
		siteInfo.poscount.push({
			posnum : i+1,
			index : index,
			posinfoindex : "0"+index
		});
	}
	goRentBikePage.html(mustache.render(siteInfoTemplate, siteInfo));
	goRentBikePage.show();
	//按钮控制
	utils.setButtonState(rentBikeBtn,true);
	
	goRentBikePage.click(function(){
		var target = $(event.target).closest('div[data-action]');
		switch(target.attr("data-action")){
			case "posSelectedAction":		//选择桩位编号
				setCurrentPos(target);
				break;
			case "rentBikeAction":			//租车
				goRentBike();
				break;
			break;
			default:
			setCurrentPos();
		}
	});
	
//	加载实时车位数据
	bike.getPointInfo(siteInfo.number)
	.done(function(response){
		if(err.isOk(response.retcode)){
			if(eap.isEmpty(response.posinfos)){
				return;
			}
			var posinfos = response.posinfos.split(",");
			var posinfoMapping = {};
			eap.each(posinfos,function(posinfo){
				if(!eap.isEmpty(posinfo)){
					posinfoMapping[posinfo.substr(0,3)] = posinfo.substr(3);
				}
			});
			$("#bikePosPanel").find("div[data-posinfo]").each(function(){
				var index = $(this).attr("data-posinfo");
				if(isBikeId(posinfoMapping[index])){
					$(this).show();
				}
			});
		}else {
			utils.msg("加载车位信息失败")		
		}
	})
	.fail(function(){
		utils.msg("加载车位信息失败")		
	})
	.always(utils.hideLoading);
	
	// 去租车
	function goRentBike(){
		if(siteInfo.distance_ > 500){
			alert("您距离站点["+siteInfo.address+"]超过了500米,无法远程租车");
			return;
		}
		var parkingId_ = getPosNum();
		if (eap.isEmpty(parkingId_)) {
			utils.msg("请选择车位...");
			return;
		}
		var html = [
			"<div class=\"tips-content-rent\"><div class=\"list-block media-list\"><ul>",
                "<li class=\"item-content\"><div class=\"item-inner\">",
                    "<span class=\"item f9 icon-color5\">网点名称: </span>",
                    "<span class=\"item i-font f9\">",siteInfo.name,"[",siteInfo.number,"]</span>",
                "</div></li>",
                "<li class=\"item-content\"><div class=\"item-inner\">",
                    "<span class=\"item f9 icon-color5\">&nbsp;&nbsp;&nbsp;&nbsp;车位号: </span>",
                    "<span class=\"item i-font f9\">",parkingId_,"</span>",
                "</div></li>",
			"</ul></div></div>"
		]
		
		utils.dialog({
			title : "租车信息提醒",
			msg : html.join("")
		}, function(result) {
			if (result) {
				// 发送租车请求
				utils.setButtonState(rentBikeBtn,true);
				bike.rentBike(siteInfo.number, parkingId_)
				.done(function(res) {
					if(!err.isOk(res.retcode)){
						switch (res.retcode) {
							case err.SERVER_ERR.code:
							utils.msg("回话失效,请重新进入公众号...");
							break;
							case err.USER_NOT_LOGIN.code:
							utils.msg("用户未登录");
							setTimeout(function(){
								location.href = "login.html"
							},1000)
							break;
							default:
								utils.msg(err.findDescByCode(res.bizstatus));
						}
					} else {
						switch(res.bizstatus){
							case err.CARD_NEW.code:
							case err.CARD_LOGOFF.code:
							utils.notice("未支开通租车业务");
							setTimeout(function(){
								location.href = "cashDeposit.html"
							},1000)
							return;
							case err.CARD_OPEN_PAYING.code:
							case err.CARD_OVERTIME_PAYING.code:
							utils.notice("有未支付订单");
							setTimeout(function(){
								location.href = "orderlist.html"
							},1000)
							return;
							case err.SUCESS.code:
							//utils.msg("租车请求发送成功");
							setTimeout(function(){
								history.back()
							},1000)
							return;
						}
						utils.notice(err.findDescByCode(res.bizstatus));
					}
				})
				.fail(function(){
					utils.msg("服务不可用，请稍候再试……");
				})
				.always(function(){
					utils.setButtonState(rentBikeBtn,false);
				})
			}
		}).fail(function() {
			utils.msg("请尝试其他车位...");
		})
	}
	
//	工具方法
	function getIndex(i) {
		return i < 10 ? "0" + i : i;
	}
	
	function isBikeId(bikeId) {
		return !eap.isEmpty(bikeId) && bikeId != "00000000" && bikeId != "FFFFFFFF";
	}
	
	function getPosNum(){
		return currentPosDiv != null ? currentPosDiv.children().eq(0).attr("data-posnum") : "";
	}
	
	function setCurrentPos(current){
		if(currentPosDiv != null){
			currentPosDiv.removeClass("round-selected");
		}
		if(current){
			currentPosDiv = current;
			currentPosDiv.addClass("round-selected");
			$("#posNumberSelectedDiv").html(getPosNum()+"号");
			utils.setButtonState(rentBikeBtn,false);
		} else {
			currentPosDiv = null;
			$("#posNumberSelectedDiv").html("请选择车位编号");
			utils.setButtonState(rentBikeBtn,true);
		}
	}
	
	function checkBizInfo(bizInfo){
		var bizInfo = bizInfo || bike.getCurrentBizInfo();
		if(bizInfo == null){
			utils.msg("系统初始化中...");
			return false;
		}
		if(!err.isOk(bizInfo.retcode)){
			switch (bizInfo.retcode) {
				case err.SERVER_ERR.code:
				utils.msg("回话失效,请重新进入公众号...");
				break;
				case err.USER_NOT_LOGIN.code:
				utils.msg("用户未登录");
				setTimeout(function(){
					location.href = "login.html"
				},1000)
				break;
				default:
					utils.msg(err.findDescByCode(res.bizstatus));
			}
			return false;
		} else {
			switch (bizInfo.bizstatus) {
				case err.CARD_NEW.code :
				case err.CARD_LOGOFF.code :
					utils.msg("未开通租车业务，不能租车");
					setTimeout(function(){
						location.href = "cashDeposit.html"
					},1000)
				return false;
				case err.CARD_OPEN_PAYING.code :
				case err.CARD_OVERTIME_PAYING.code :
					utils.msg("有未支付订单，不能租车");
					setTimeout(function(){
						location.href = "orderlist.html"
					},1000)
				return false;
			}
		}
		return true;		
	}
});