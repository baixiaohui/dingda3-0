require(["jquery","eap","mustache","bike","utils"],function($,eap,mustache,bike,utils) {
	var NearSitesList = $("#NearSitesList");
	var NearSitesTemplate = NearSitesList.html();
	var siteData;
	//加载数据
	bike.queryNearSites()
	.done(function(response){
		siteData = response.data;
		NearSitesList.html(mustache.render(NearSitesTemplate, {
			data : siteData
		}));
		NearSitesList.show();
	})
	.fail(function(){
		siteData = null;
		utils.msg("周边网点查询失败...")
	})
	.always(utils.hideLoading);
	//去租车
	NearSitesList.click(function(){
	   	var target = $(event.target).closest('a[data-number]');
		if(target.length == 0){
			return;
		}
		if(siteData == null){
			return;
		}
		var siteNumber = target.attr("data-number");
		if(eap.isEmpty(siteNumber)){
			return;
		}
		var targetSiteData;
		eap.each(siteData,function(data){
			if(data.number == siteNumber){
				targetSiteData = data;
				return false;
			}
		});
		window.location.href = "rent.html?"+eap.urlEncode(targetSiteData);
	});
});