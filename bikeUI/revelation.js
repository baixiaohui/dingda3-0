require(["jquery","eap","mustache","bike","err","utils"],function($,eap,mustache,bike,err,utils) {
	var submitEmailBtn = $("#submitEmailBtn");
	submitEmailBtn.click(function(){
		var title = $("#emailTitle").val();
		var content = $("#emailContent").val();
		if(eap.isEmpty(title) || eap.isEmpty(content)){
			utils.msg("爆料标题和内容均不能为空");
			return;
		}
		utils.setButtonState(submitEmailBtn,true);
		bike.submitMail(title,content)
		.done(function(res){
			if(err.isOk(res.retcode)){
				history.back();
			} else {
				utils.msg(res.retmsg);
			}
		})
		.always(function(){
			utils.setButtonState(submitEmailBtn,false);
		});
	});
});