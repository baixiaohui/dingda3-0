require(["jquery","eap","mustache","bike","err","utils"],function($,eap,mustache,bike,err,utils) {
	var userInfo = eap.urlDecode(window.location.search.substring(1));
	var logoutBtn = $("#logoutBtn");
	if(!eap.isEmpty(userInfo.userid)){
		$("#userNameSpan").html(userInfo.userid);
	}
	if(!eap.isEmpty(userInfo.phonenum)){
		var phoneNum = [];
		for(var i= 0 ;i < userInfo.phonenum.length;i++){
			if(i > 2 && i < 7){
				phoneNum.push('*');
			} else {
				phoneNum.push(userInfo.phonenum.charAt(i));
			}
		}
		$("#userPhoneSpan").html(phoneNum.join(""));
	}
	logoutBtn.click(function(){
		utils.setButtonState(logoutBtn,true);
		bike.logout()
		.done(function(res){
			if(err.isOk(res.retcode)){
				window.location.href = "index.html";
			} else {
				utils.msg(res.retmsg);
			}
		})
		.always(function(){
			utils.setButtonState(logoutBtn,false);
		});
	});
});