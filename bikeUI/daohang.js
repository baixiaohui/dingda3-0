require(["eap","md5","utils","err"],function(eap,md5,utils,err) {
	 var cliwidth=$(window).width();
	 var cliheight=$(window).height();
	 var imgArray1=["image/icon_bike0@2x.png","image/icon_bike1@2x.png","image/icon_bike1@2x.png"];
	 var pArray1=["助力车","无桩车","有桩车"];
	 var imgArray2=["image/icon_bike0@2x.png","image/icon_bike1@2x.png","image/icon_bike1@2x.png"];
	 var pArray2=["帮助指南","用户手册","问题反馈"];
	 var view={
		  allmap:{
		    getSelect:function(){
		      return '#map';
		    },
		  },
	    pagetwo:{
	  	  getSelect:function(){
		  		  return '#pagetwo'
		  	 },
		  },
		  pagetwo:{
	  	  getSelect:function(){
		  		  return '#pagetwo'
		  	 },
		  },
		  guideRoad:{
		  	getSelect:function(){
		  		return '#guideRoad';
		  	},
		  },
	 };
	 var show={
	   guideBoxInit:function(){
		 	 var box=$(view.guideRoad.getSelect()).empty();
		   var jsonData={
			     	"roadname":"滨文路",
			     	"kilometer":666,
			     	"acatime":10,
			     	"bikenum":18,
			     	"parking":9
			     };
	     var imgArr=[
	     {"src":"image/icon_location1@2x.png"},
	     {"src":"image/icon_location1@2x.png"},
	     {"src":"image/icon_location1@2x.png"}
	     ];
	   
      var div1=$("<div>").addClass("first");
      var div1_a=$("<div>").addClass("line-one");
      var div1_a1=$("<div>").addClass("text-line");
      div1_a1.append($("<img>").attr("src","image/icon_location1@2x.png"));
      div1_a.append(div1_a1);
      var div1_a2=$("<div>").addClass("roadName").text(jsonData.roadname);
      div1_a.append(div1_a2);
      var div1_b=$("<div>").addClass("line-two");
      div1_b.append($("<a>").attr("href","##").text("开始导航"));
      div1.append(div1_a).append(div1_b);

      var div2=$("<div>").addClass("second");
      var div2_a=$("<div>").addClass("second-one");
      var div2_a1=$("<div>").addClass("text-line");
      div2_a1.append($("<img>").attr("src","image/icon_diatance0@2x.png"));
      var div2_a2=$("<div>").addClass("road").text(jsonData.kilometer + "m");
      div2_a.append(div2_a1).append(div2_a2);  
      div2.append(div2_a);
 
      var div2_b=$("<div>").addClass("second-two");
      var div2_b1=$("<div>").addClass("text-line");
      div2_b1.append($("<img>").attr("src","image/icon_time0@2x.png"));
      var div2_b2=$("<div>").addClass("road").text(jsonData.acatime +"分钟");
      div2_b.append(div2_b1).append(div2_b2);  
      div2.append(div2_b);

      var div3=$("<div>").addClass("three");
      var div3_a=$("<div>").addClass("three-one");
      var div3_a1=$("<div>").addClass("text-line");
      div3_a1.append($("<img>").attr("src","image/icon_have0@2x.png"));
      var div3_a2=$("<div>").addClass("road").text(jsonData.bikenum);
      div3_a.append(div3_a1).append(div3_a2);  
      div3.append(div3_a);
 
      var div3_b=$("<div>").addClass("three-two");
      var div3_b1=$("<div>").addClass("text-line");
      div3_b1.append($("<img>").attr("src","image/icon_stop0@2x.png"));
      var div3_b2=$("<div>").addClass("road").text(jsonData.parking);
      div3_b.append(div3_b1).append(div3_b2);  
      div3.append(div3_b);


       
        box.append(div1).append(div2).append(div3).show();
		 }
	 };
	 var ctrl={
			init:function(){
				show.guideBoxInit();
		    var itemFunc = null;
		    for (var event in ctrl.bindEvent) {
		        itemFunc = ctrl.bindEvent[event]
		        if (itemFunc && typeof (itemFunc) === "function") {
		            itemFunc();
		        };
		    };
			},
			bindEvent:{
			 
			   },
		 
	};
	 ctrl.init();
});

