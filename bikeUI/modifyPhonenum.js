require(["jquery","eap","mustache","bike","utils","err"],function($,eap,mustache,bike,utils,err) {
  	//更换手机号码
  	$("#modifyPhonenumBtn").click(function(){
		var userNewPhone = $("#userNewPhone").val();
		var checkcode = $("#checkcode").val();
		utils.setButtonState(this,true);
		if(eap.isEmpty(userNewPhone)){
			utils.msg("请输入手机号");
			utils.setButtonState(this,false);
   		 	return;
		} 
		if(checkcode == ""){
			utils.msg("请输入验证码");
			utils.setButtonState(this,false);
			return;
		}
		utils.msg("正在为您更换手机号码请稍后...");
		bike.modifyPhonenum(userNewPhone,checkcode)
		.done(function(res){
			if(err.isOk(res.retcode)){
				window.location.href = "mine.html";
			} else {
				utils.msg(res.retmsg);
			}
		})
		.always(function(){
			utils.setButtonState(this,false);
		});
	});
	
	var task;
	var taskConfig = {
		timeCount:30,
	    run: function(){
	    	if(this.timeCount > 0){
	    		$("#checkcodebtn").attr('disabled',true);
	    		$("#checkcodebtn").html(this.timeCount+"s后获取");
	    	} else if(this.timeCount == 0){
	    		eap.TaskMgr.stop(task);
	    	}
	    	this.timeCount--;
	    },
	    onStop : function(){
	    	this.timeCount = 30;
	    	$("#checkcodebtn").attr('disabled',false).html("发送验证码");
	    },
	    interval: 1000 //1 second
	}
	
	//获取重置手机号码验证码
	$("#checkcodebtn").click(function(){
		var userNewPhone = $("#userNewPhone").val();
		if(eap.isEmpty(userNewPhone)){
			utils.msg("手机号码不能为空");
			return;
		}
		bike.getRegistVCode(userNewPhone)
		.done(function(response){
			if(err.isOk(response.retcode)){
				task = eap.TaskMgr.start(taskConfig);
			}
			utils.msg(response.retmsg);
		})
		.fail(function(response){
			utils.msg(response.retmsg);
			eap.TaskMgr.stop(task);
		});
	});

});	