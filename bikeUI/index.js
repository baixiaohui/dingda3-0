require(["jquery","eap","mustache","bike","md5","utils","err","weixin"],function($,eap,mustache,bike,md5,utils,err,wx) {
	if(!BMap || !BMap.Geolocation) {
		alert("地图加载失败...")
		return;
	}
	var 
	map,		/*地图对象*/
	lastCp,		/*最后加载数据位置*/
	curCity,	/*当前城市*/
//	curPersonPosition,	/*当前人员位置*/
	curCenterMarker,	/*当前屏幕中心坐标*/
	curPersonMarker,	/*当前人员位置覆盖物*/
	curSiteMarker,		/*当前站点标注*/
	curSitesData,		/*当前周边网点数据*/
	disableMoveEvent	/*禁用moveend标志位*/,
	moveStartTime = 0;

	bike.getBizInfo()
	bike.getLocation()
	.done(initMap)
	
	.fail(function(){
		utils.msg("周边网点查询失败...")
	})
	.always(utils.hideLoading);
	var isMapinted = false;
	function initMap(loc) {
		if(isMapinted){
			return;
		}
		isMapinted = true;
		// 初始化Map
		
		point = new BMap.Point(loc.lon, loc.lat);
		map = new BMap.Map("mapdiv");
		map.addControl(new BMap.ScaleControl());
		// map.addControl(new BMap.NavigationControl());
		map.addControl(new GeolocationControl());
		map.addControl(new ZoomControl());
		map.addControl(new SaomiaoControl());
		map.addControl(new LoginControl());
		map.centerAndZoom(point, 16);
		// 注册事件
		map.addEventListener("movestart", function() {
			moveStartTime = (new Date()).getTime(); 
		});
		
		map.addEventListener("moveend", function() {
			if(disableMoveEvent === true) return;
			if((new Date()).getTime() - moveStartTime < 200){
				onMapclick();
			} else if(!$("#siteInfo").is(":visible")){
				loadBikepoint();
			}
		});
		map.addEventListener("click",onMapclick);
		//初始化当前坐标
		setMarker(point);
		//加载网点
		loadBikepoint();
	}
	
	// 加载网点
	function loadBikepoint() {	
		//标注我的位置
		var cp = map.getCenter();
		var tooFarAway = true;
		if(lastCp != null){//是否需要加载数据
			var distance = map.getDistance(cp, lastCp);
			tooFarAway = distance > 500;
		}
		if(tooFarAway === false){
			return;
		}
		returnToOriginal();
		//清理所有网点标注
		eap.each(map.getOverlays(),function(marker){
			if(marker.number){
				marker.removeEventListener("click", onSiteMarkClick); 
				map.removeOverlay(marker);
				eap.destory(marker);
			}
		});
		//
		lastCp = cp;
		bike.queryNearSites({
			lon : cp.lng,
			lat : cp.lat
		},{
			lon : curPersonMarker.getPosition().lng,
			lat : curPersonMarker.getPosition().lat
		})
		.done(function(response){
			eap.destory(curSitesData);
			curSitesData = response.data;
			eap.each(curSitesData,function(bikePoint) {
				var percent = 0,
				restorecount = parseInt(bikePoint.restorecount),
				rentcount = parseInt(bikePoint.rentcount),
				allCount = restorecount + rentcount;
				
				var totalcount = parseInt(bikePoint.totalcount);
				totalcount = (totalcount == 0 || totalcount < allCount) ? allCount : totalcount;
				bikePoint.totalcount = totalcount;
				if(totalcount > 0){
					percent = parseInt(Math.floor(rentcount*10/totalcount)*10);
				}
				if(percent == 0 && rentcount > 0){//只要有一条可租就不是零储
					percent = 10;
				}
				var icon = "error";
				var color = "icon-color11";
				if(bikePoint.status == "9"){//7=正常，8=异常，9=断线
					icon = "&#xe61e;";
					color = "icon-color2";
				}else if(percent == 0){
					icon = "&#xe631;";
				} else if(percent <= 30){
					icon = "&#xe630;";
				} else if(percent > 30 && percent <= 60){
					icon = "&#xe62e;";
				} else if(percent > 60 && percent <= 90){
					icon = "&#xe61f;";
				} else {
					icon = "&#xe62f;";
				}
				var marker = new BMap.Label(
					[
						"<div class='mapIcon-layer1'><i class=\"icon iconfont\">&#xe633;</i></div>",
						"<span class=\"", color,"\"><i data-number='", bikePoint.number,"' class=\"icon iconfont\">", icon,"</i></span>"
					].join(""), {
					offset: new BMap.Size(-16,-20),
				    position : new BMap.Point(bikePoint.lon,bikePoint.lat)
				});
				marker.setStyle({
					background: "rgba(255,255,255,0)",
					border :"0px",
					fontSize:"39px"
				});
				marker.number = bikePoint.number;
				marker.addEventListener("click",onSiteMarkClick);
				map.addOverlay(marker);
			});
		}).fail(function(){
			utils.msg("网点数据加载失败...");
		});
	}
	
	//标注点击事件
	var curCircle;
	function onMapclick(){
		var marker = curSiteMarker;
		$("#siteInfo").hide();
		if(marker){
			marker.setStyle({
				fontSize : "32px"
			});
			marker.setOffset(new BMap.Size(-16,-20));
		}
		if(curCircle != null){
			map.removeOverlay(curCircle);
		}
		curSiteMarker = null;
	}
	
	function setCurMarker(marker,callback){
		disableMoveEvent = true;
		curSiteMarker = marker;
		marker.setStyle({
			fontSize:"40px"
		});
		marker.setOffset(new BMap.Size(-21,-25));
		//
		if(curCircle != null){
			map.removeOverlay(curCircle);
		}
		curCircle = new BMap.Circle(curSiteMarker.getPosition(),300);
		curCircle.setFillOpacity(0.2);
		curCircle.setFillColor("#3399ff");
		curCircle.setStrokeColor("");
		map.addOverlay(curCircle);
		//
		showSiteInfo();
		
		if(eap.isFunction(callback)){
			callback();
		}
		disableMoveEvent = false;
	}
<<<<<<< HEAD
	$("#siteInfo").click(function(){
		window.location.href = "daohang.html";
	});
=======
	// $("#siteInfo").click(function(){
	// 	var target = $(event.target).closest('a[data-number]');
	// 	if(target.length == 0){
	// 		return;
	// 	}
	// 	var siteData = getSelectedSiteData();
	// 	if(siteData == null) return;
	// 	window.location.href = "rent.html?"+eap.urlEncode(siteData);
	// });
>>>>>>> 60ceda806930040572d85629859f462cc0f8d39c
	
	function onSiteMarkClick(e){
		selectNetpoint(e.target);
	}
	
	function selectNetpoint(marker,callback) {
		returnToOriginal();
		setCurMarker.defer(100,null,[marker,callback]);
	}
	function returnToOriginal(){
		var marker = curSiteMarker;
		$("#siteInfo").hide();
		if(marker){
			marker.setStyle({
				fontSize : "32px"
			});
			marker.setOffset(new BMap.Size(-16,-20));
		}
		if(curCircle != null){
			map.removeOverlay(curCircle);
		}
		curSiteMarker = null;
	}
	
	function setMarker(point){
		if(curPersonMarker != null){
			map.removeOverlay(curPersonMarker);
		}
//		当前人员位置
		curPersonMarker = new BMap.Label(
			[
				"<div class='mapIcon-layer1'><i class=\"icon iconfont\">&#xe633;</i></div>",
				"<span class='icon-color3'><i class=\"icon iconfont\">&#xe620;</i></span>"
			].join(""),{
				offset: new BMap.Size(-38,-28),
			    position : point
			}
		);
		curPersonMarker.setStyle({
			background: "rgba(255,255,255,0)",
			fontSize:"32px",
			border :"0"
		});
		map.addOverlay(curPersonMarker);
	}
	
	var siteInfoTemplate = $("#siteInfo").html();
	function showSiteInfo(){
		var siteData = getSelectedSiteData();
		if(siteData == null) return;
		$("#siteInfo").slideDown(200);
		$("#siteInfo").html(
			mustache.render(siteInfoTemplate, siteData)
		); 
	}
	
	function getSelectedSiteData(){
		if(curSiteMarker == null){
			return;
		}
		return getSiteDataById(curSiteMarker.number);
	}
	
	function getSiteDataById(id){
		var result = null;
		eap.each(curSitesData,function(siteData){
			if(siteData.number == id){
				result = siteData;
				return false;
			}
		});
		return result;
	}
	///
	function GeolocationControl(){    
	    this.defaultAnchor = BMAP_ANCHOR_BOTTOM_RIGHT;    
	    this.defaultOffset = new BMap.Size($(window).width() - 70, 30);    
	}    
	GeolocationControl.prototype = new BMap.Control();
	GeolocationControl.prototype.initialize = function(map) {
		var div = document.createElement("div");
		div.innerHTML = [
			'<div class="ui-block-table-child">' ,
				'<img src="image/icon_location0@3x.png" style="width:50px;height=50px">',
			'</div>'
		].join("");
		div.onclick = function(e) {
//			$(div).select(".iconfont").html("...");
			bike.getLocation()
			.done(function(loc){
				var point = new BMap.Point(loc.lon, loc.lat);
				setMarker(point);
				map.centerAndZoom(point, 16);
			})
			.fail(function(){
				utils.msg("获取定位信息失败...")
			})
			.always(function(){
//				$(div).select(".iconfont").html("&#xe621;");
			});
		}
		map.getContainer().appendChild(div);
		return div;
	}
	function ZoomControl(){
      // 默认停靠位置和偏移量
      this.defaultAnchor = BMAP_ANCHOR_BOTTOM_RIGHT;
      this.defaultOffset = new BMap.Size(20, 30); // 距离左上角位置
    }
     ZoomControl.prototype = new BMap.Control();
    ZoomControl.prototype.initialize = function(map){
      // 创建一个DOM元素
      var div = document.createElement("div");
      div.innerHTML = [
			'<div class="ui-block-table-child">',
	 	  		'<img src="image/icon_bike@3x.png" style="width:50px;height=50px">',
	 		'</div>'
		].join("");
      div.onclick = function(e){
        if(!checkLogin()){return;}
        $("#mengcengmc").slideDown(20);
        $("#infobottom").slideDown(20);
		// var bizInfo = bike.getCurrentBizInfo();
		// location.href = "mine.html?userid="+(bizInfo.userid||"")+"&phonenum="+(bizInfo.phonenum||"");
		// $("#mengcengmc").show(200);
      }
      // 添加DOM元素到地图中
      map.getContainer().appendChild(div);
      // 将DOM元素返回
      return div;
    }
    //蒙层隐藏
    $("#mengcengmc").click(function(){
       	mengcengYincang();
       	$("#personitem").hide();
    });
    $("#baozhengjin").click(function(){
    	mengcengYincang();
    	window.location.href = "cashDeposit.html";
    });
    $("#orderListnew").click(function(){
    	mengcengYincang();
    	window.location.href = "orderlist.html";
    });
    $("#rentListnew").click(function(){
    	mengcengYincang();
    	window.location.href = "recordlist.html";
    });
    //
    $("#kefuclass").click(function(){
    	mengcengYincang();
    	window.location.href = "";
    });
    $("#personpoint").click(function(){
    	mengcengYincang();
    	var bizInfo = bike.getCurrentBizInfo();
		location.href = "mine.html?userid="+(bizInfo.userid||"")+"&phonenum="+(bizInfo.phonenum||"");
    });
    function mengcengYincang(){
    	$("#mengcengmc").slideUp(10);
    	$("#infobottom").slideUp(10);
    }
    function personYincang(){
    	$("#mengcengmc").slideUp(10);
    	$("#personitem").slideUp(10);
    }
    function LoginControl(){
      // 默认停靠位置和偏移量
      this.defaultAnchor = BMAP_ANCHOR_BOTTOM_RIGHT;
      this.defaultOffset = new BMap.Size(20, 90); // 距离左上角位置
    }
     LoginControl.prototype = new BMap.Control();
    LoginControl.prototype.initialize = function(map){
      // 创建一个DOM元素
      var div = document.createElement("div");
      div.innerHTML = [
			'<div class="ui-block-table-child">',
	 	  		'<img src="image/icon_people@3x.png" style="width:50px;height=50px">',
	 		'</div>'
		].join("");
      div.onclick = function(e){
        if(!checkLogin()){return;}
		$("#mengcengmc").slideDown(20);
        $("#personitem").slideDown(20);
      }
      // 添加DOM元素到地图中
      map.getContainer().appendChild(div);
      // 将DOM元素返回
      return div;
    }
    function SaomiaoControl(){
      // 默认停靠位置和偏移量
      this.defaultAnchor = BMAP_ANCHOR_BOTTOM_RIGHT;
      this.defaultOffset = new BMap.Size(0, 80); 
    }
     SaomiaoControl.prototype = new BMap.Control();
    SaomiaoControl.prototype.initialize = function(map){
      // 创建一个DOM元素
      var div = document.createElement("div");
      div.innerHTML = [
			'<div class="saomiao-size saomiao-background">',
	 	  		// '<img src="image/icon_button_bg0.png" height="50" width="150">',
	 	  		// '<div class="saomiao-imagepozision">',
	 	  		// '<a>',
	 	  		'<img src="image/icon_scan0@2x.png" class="saomiao-image-size">',
	 	  		// '</a>',
	 	  		// '</div>',
	 		'</div>'
		].join("");
      div.onclick = function(e){
      	// wx.bluetoothSaomiao();
		if(!checkLogin()){return;}
		var parkingId_ = "";
		bike.scanQRCode()
		.then(function(res){
			parkingId_ = res.parkingId;
			return bike.getSiteInfo(res.stationId);
		})
		.done(function(res){
			if(!err.isOk(res.retcode)){
				utils.msg("查询站点信息失败!");
				return;
			}
			var html = [
				"<div class=\"tips-content-rent\"><div class=\"list-block media-list\"><ul>",
                    "<li class=\"item-content\"><div class=\"item-inner\">",
                        "<span class=\"item f9 icon-color5\">网点名称: </span>",
                        "<span class=\"item i-font f9\">",res.name,"</span>",
                    "</div></li>",
				    "<li class=\"item-content\"><div class=\"item-inner\">",
                        "<span class=\"item f9 icon-color5\">网点编号: </span>",
                        "<span class=\"item i-font f9\">",res.number,"</span>",
                    "</div></li>",
				"<li class=\"item-content\"><div class=\"item-inner\">",
                        "<span class=\"item f9 icon-color5\">车位编号: </span>",
                        "<span class=\"item i-font f9\">",parkingId_,"</span>",
                    "</div></li>",
                    "<li class=\"item-content\"><div class=\"item-inner\">",
                        "<span class=\"item f9 icon-color5\">网点地址: </span>",
                        "<span class=\"item i-font f9\">",res.address,"</span>",
                    "</div></li>",
				"</ul></div></div>"
			]
			utils.dialog({
				title : "租车信息",
				msg : html.join("")
			},function(result){
				if(result){
					bike.rentBike(res.number, parkingId_)
					.done(function(res) {
						switch(res.bizstatus){
							case err.TOO_FREQUENT.code:
								utils.notice("1分钟以内不能再租车");
								return;
							case err.CARD_NEW.code:
							case err.CARD_LOGOFF.code:
								utils.notice("未支开通租车业务");
								setTimeout(function(){
									location.href = "cashDeposit.html"
								},1000)
								return;
							case err.CARD_OPEN_PAYING.code:
							case err.CARD_OVERTIME_PAYING.code:
								utils.notice("有未支付订单");
								setTimeout(function(){
									location.href = "orderlist.html"
								},1000)
								return;
							case err.SUCESS.code:
								utils.msg("租车请求发送成功");
								return;
						}
						utils.notice(err.findDescByCode(res.bizstatus));
					})
					.fail(function(){
						utils.msg("服务不可用，请稍候再试……");
					})
				}
			})
		})
		.fail(function(e){
			utils.msg("亲！您扫描的可能不是租车二维码哟...");
		})
	}
      // 添加DOM元素到地图中
      map.getContainer().appendChild(div);
      // 将DOM元素返回
      return div;
    }
    function checkLogin(){
		var bizInfo = bizInfo || bike.getCurrentBizInfo();
		if(bizInfo == null){
			utils.msg("系统初始化中...");
			return false;
		}
		if(!err.isOk(bizInfo.retcode)){
			switch (bizInfo.retcode) {
				case err.SERVER_ERR.code:
				utils.msg("回话失效,请重新进入公众号...");
				break;
				case err.USER_NOT_LOGIN.code:
				utils.msg("用户未登录");
				setTimeout(function(){
					location.href = "login.html"
				},1000)
				break;
				default:
					utils.msg(err.findDescByCode(bizInfo.bizstatus));
			}
			return false;
		}
		return true;
	}
});