require(["jquery","eap","mustache","bike","utils","err"],function($,eap,mustache,bike,utils,err) {
  		//修改密码
	$("#changePassWordBtn").click(function(){
		var me = this;
		var oldPwd = $("#oldPwd").val();
		var fristNewPwd = $("#fristNewPwd").val();
		var secondNewPwd = $("#secondNewPwd").val();
		utils.setButtonState(me,true);
		if(eap.isEmpty(oldPwd)){
			utils.msg("请输入原始密码");
			utils.setButtonState(me,false);
   		 	return;
		} 
		if(eap.isEmpty(fristNewPwd) || eap.isEmpty(secondNewPwd)){
			utils.msg("请输入新密码");
			utils.setButtonState(me,false);
			return;
		}
		if(fristNewPwd != secondNewPwd){
			utils.msg("两次输入密码不一致");
			utils.setButtonState(me,false);
			return;
		}
		//utils.msg("正在为您修改密码请稍后...");
		
		bike.updatepwd(oldPwd,fristNewPwd)
		.done(function(res){
			if(err.isOk(res.retcode)){
				utils.msg("修改密码成功");
				setTimeout(function(){
					location.href = "index.html";
				},1000)
			} else {
				utils.msg(res.retmsg);
			}
		})
		.always(function(){
			utils.setButtonState(me,false);
		});
	});
});