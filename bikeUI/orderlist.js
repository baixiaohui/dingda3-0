require(["jquery","eap","mustache","bike","pingpp","utils","err"],function($,eap,mustache,bike,pingpp,utils,err) {
	var OrderList = $("#OrderList");
	var OrderListTemplate = OrderList.html();
	//加载数据
	bike.queryOrders()
	.then(function(res){
		eap.each(res.data,function(data){
			data.ordermoney = data.ordermoney/100
			data.orderstatus_txt = 
			data.orderstatus == "1" ? "新建":
			data.orderstatus == "2" ? "已提交" :
			data.orderstatus == "3" ? "已完成" :
			data.orderstatus == "4" ? "已取消" : ""
			data.display = data.orderstatus == "1" || data.orderstatus == "2" ? "block":"none";
			data.refundflag_txt = data.refundflag == 1 ? "[已申请退款]" : "";
		});
		return res;
	})
	.done(function(res){
		if(res.retcode == err.USER_NOT_LOGIN.code){
			window.location.href = "login.html";
			return;
		}
		OrderList.html(mustache.render(OrderListTemplate, {
			data : res.data
		}));
		OrderList.show();
		utils.hideLoading();
	})
	.fail(function(){
		utils.msg("订单记录查询失败...")
	})
	.always(utils.hideLoading);
	
	OrderList.click(function(e){
		var target = $(event.target).closest('p[data-orderid]');
		if(target.length == 0){
			return;
		}
		var orderid = target.attr("data-orderid");
		if(!eap.isEmpty(orderid)){
			utils.msg("获取支付数据...");
			bike.getCharge(orderid)
			.done(function(res){
				if(err.isOk(res.retcode)){
					utils.msg("调起支付控件...");
					bike.createPayment(res.chargedata,function(result, err){
						if (result == "success") {
							bike.checkOrder(orderid)
							.done(function(res){
								location.reload();
							})
						} else {
							utils.msg("支付失败");
						}
					});
				} else {
					utils.msg(res.retmsg);
				}
			})
			.fail(function(){
				utils.msg("获取支付数据失败");
			})
		}
	});
});