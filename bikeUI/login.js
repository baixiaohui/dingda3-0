require(["jquery","eap","mustache","bike","utils","err"],function($,eap,mustache,bike,utils,err) {
	var loginBtn = $("#loginBtn");
	loginBtn.click(function(){
		var userName = $("#userNameText").val();
		var passwd = $("#passwdText").val();
		if(eap.isEmpty(userName) || eap.isEmpty(passwd)){
			utils.msg("登录用户名或密码不能为空");
			return;
		}
		utils.setButtonState(loginBtn,true);
		bike.login(userName,passwd)
		.done(function(res){
			if(err.isOk(res.retcode)){
				window.location.href = "index.html";
			} else {
				utils.msg(res.retmsg);
			}
		})
		.always(function(){
			utils.setButtonState(loginBtn,false);
		});
	});
});