require(["jquery","eap","bike","utils","eap","err"],function($,eap,bike,utils,eap,err) {
	//注册
	$("#userRegisterBtn").click(function(){
		if(!$('#protoclCheck').is(':checked')){
			utils.msg("请同意<<在线租车服务协议>>");
			return;
		}
		var userPhone = $("#userPhone").val();
		var firstpwd = $("#firstpwd").val();
		var secondpwd = $("#secondpwd").val();
		var checkcode = $("#checkcode").val();
		var me = this;
		//var reg = /^[\w]{6,12}$/;
		
		if(userPhone==""){
			utils.msg("请输入手机号");
   		 	return;
		} 
		if(checkcode == ""){
			utils.msg("请输入验证码");
			return;
		}
		if(firstpwd == "" || secondpwd == ""){
			utils.msg("请输入密码 ");
   		 	return;
		} 
		if(firstpwd.length < 6){
			utils.msg("密码格式不正确，至少是六位 ");
   		 	return;
		}
		if(firstpwd != secondpwd){
			utils.msg("两次输入密码不一致");
			return;
		}
		utils.setButtonState(this,true);
		utils.msg("正在注册...");
		bike.register(userPhone,firstpwd,checkcode)
		.done(function(res){
			if(err.isOk(res.retcode)){
				utils.msg("账号注册成功!");
				setTimeout(function(){
					location.href = "login.html"
				},2000)
			} else {
				utils.msg(res.retmsg);
				utils.setButtonState(me,false);
			}
		})
		.fail(function(){
			utils.msg("正在注册失败...");
			utils.setButtonState(me,false);
		})
		.always(function(){
			utils.setButtonState(me,false);
		});
		
	});
	var task;
	var taskConfig = {
		timeCount:30,
	    run: function(){
	    	if(this.timeCount > 0){
	    		$("#checkcodebtn").attr('disabled',true);
	    		$("#checkcodebtn").html(this.timeCount+"s后获取");
	    	} else if(this.timeCount == 0){
	    		eap.TaskMgr.stop(task);
	    	}
	    	this.timeCount--;
	    },
	    onStop : function(){
	    	this.timeCount = 30;
	    	$("#checkcodebtn").attr('disabled',false).html("发送验证码");
	    },
	    interval: 1000 //1 second
	}
	
	//获取注册验证码
	$("#checkcodebtn").click(function(){
		var userPhone = $("#userPhone").val();
		if(eap.isEmpty(userPhone) || userPhone.length != 11){
			utils.msg("手机号码不能为空");
			return;
		}
		if(userPhone.length != 11){
			utils.msg("手机号码长度不正确");
			return;
		}
		task = eap.TaskMgr.start(taskConfig);
		
		bike.getRegistVCode(userPhone)
		.done(function(response){
			if(err.isOk(response.retcode)){
				utils.msg(response.retmsg);
			} else {
				utils.msg(response.retmsg);
				eap.TaskMgr.stop(task);
			}
		})
		.fail(function(response){
			utils.msg(response.retmsg);
			eap.TaskMgr.stop(task);
		});
	});
	
	//
	$("#protoclHref").click(function(){
		$("#protocolDiv").show();		
	});
});