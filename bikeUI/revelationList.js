require(["jquery","eap","mustache","bike","err","utils"],function($,eap,mustache,bike,err,utils) {
	var revelationList = $("#revelationList");
	var OrderListTemplate = revelationList.html();
	//加载数据
	bike.listMails()
	.then(function(res){
		eap.each(res.data,function(data){
			if(eap.isEmpty(data.reply)){
				data.reply_txt = "暂未回复";
				data.reply_style = "icon-color5";
			} else {
				data.reply_txt = data.reply;
				data.reply_style = "icon-black";
			}
		});
		return res;
	})
	.done(function(res){
		if(res.retcode == err.USER_NOT_LOGIN.code){
			window.location.href = "login.html";
			return;
		}
		revelationList.html(mustache.render(OrderListTemplate, {
			data : res.data
		}));
		revelationList.show();
		utils.hideLoading();
	})
	.fail(function(){
		utils.msg("数据查询失败...");
	})
	.always(utils.hideLoading);
});