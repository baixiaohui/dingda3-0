define(['jquery',"eap","weixin","gps","utils","md5"],function($,eap,weixin,gps,utils,md5){
	var currentBizInfo = {};
	return {
		login : function(userName,passWord){
			return $.ajax({type: "POST",dataType:"json",
				url: contextPath + "rest/bikemt/security/clientlogin",
				data : {
					userName : userName,
					passWord : md5.calcMD5(passWord)
				}
			});
		},
		logout : function(userName,passwd){
			return $.ajax({type: "POST",dataType:"json",
				url: contextPath + "rest/bikemt/security/clientlogout"
			});
		},
		register : function(phoneNumber,password,vcode){
			return $.ajax({type: "POST",dataType:"json",
				url: contextPath + "rest/bikemt/member/register",
				data : {
					phoneNumber : phoneNumber,
					password : md5.calcMD5(password),
					vcode : vcode
				}
			});
		},
		updatepwd : function(oldPassword,newPassword){
			return $.ajax({type: "POST",dataType:"json",
				url: contextPath + "rest/bikemt/member/updatepwd",
				data : {
					oldPassword : md5.calcMD5(oldPassword),
					newPassword : md5.calcMD5(newPassword)
				}
			});
		},
		resetpwd : function(phoneNumber,newPassword,vcode){
			return $.ajax({type: "POST",dataType:"json",
				url: contextPath + "rest/bikemt/member/resetpwd",
				data : {
					phoneNumber : phoneNumber,
					newPassword : md5.calcMD5(newPassword),
					vcode : vcode
				}
			});
		},
		modifyPhonenum : function(phoneNumber,vcode){
			return $.ajax({type: "POST",dataType:"json",
				url: contextPath + "rest/bikemt/member/bindnewphone",
				data : {
					phoneNumber : phoneNumber,
					vcode : vcode
				}
			});
		},
		getResetPWDVCode : function(phoneNumber){
			return $.ajax({type: "POST",dataType:"json",
				url: contextPath + "rest/bikemt/security/getresetpwdvcode",
				data : {
					appid : appId,
					phoneNumber : phoneNumber
				}
			});
		},
		getRegistVCode : function(phoneNumber){
			return $.ajax({type: "POST",dataType:"json",
				url: contextPath + "rest/bikemt/security/getregistvcode",
				data : {
					appid : appId,
					phoneNumber : phoneNumber
				}
			});
		},
		//充电业务
		getCurrentBizInfo : function(){return currentBizInfo;},
		getBizInfo : function(){
			return $.ajax({type: "POST",dataType:"json",
				url: contextPath + "rest/bikegw/charge/bizinfo"
			})
			.then(function(res){
				currentBizInfo = res;
				currentBizInfo.bizstatus = currentBizInfo.bizStatus;
				delete currentBizInfo.bizStatus;
				return res;
			});
		},
		getSiteInfo:function(stationId){
			return $.ajax({type: "POST",dataType:"json",
				url: contextPath + "rest/bikegw/charge/netpoints/status",
				data : {
					stationId : stationId
				}
			})
		},
		queryNearSites : function(){
			var d = $.Deferred();
			var map = new BMap.Map();
			weixin.getLocation()
			.done(function(res){
				var bdgps = gps.gpsToBaiduPoint(parseFloat(res.latitude),parseFloat(res.longitude));
				$.ajax({type: "POST",dataType:"json",
					url: contextPath + "rest/bikegw/charge/netpoints/nearstationstatus",
					data : {
						lon : bdgps.lon,
						lat : bdgps.lat,
						len : 3000
					},
					success : function(res){
						eap.each(res.data,function(data){
							data.distance_ = map.getDistance(new BMap.Point(bdgps.lon,bdgps.lat), new BMap.Point(data.lon,data.lat));
							data.distance = utils.formatDistance(data.distance_);
						});
						
						res.data.sort(function(a,b){
							return a.distance_ - b.distance_;
						});
						
						d.resolve(res);
					},
					error : d.reject
				});
			})
			.fail(d.reject);
			return d;
		},
		queryRecords : function(retcount){
			return $.ajax({type: "POST",dataType:"json",
				url: contextPath + "rest/bikegw/charge/records",
				data : {
					retcount : retcount || "5"
				}
			})
		},
		queryOrders : function(retcount){
			return $.ajax({type: "POST",dataType:"json",
				url: contextPath + "rest/bikegw/charge/getorders",
				data : {
					retcount : retcount || "5"
				}
			})
		},
		requestCharge : function(stationId,parkNum){
			return $.ajax({type: "POST",dataType:"json",
				url: contextPath + "rest/bikegw/charge/request",
				data : {
					stationId : stationId,
					parkNum : parkNum
				}
			})
			.then(function(res){
				currentBizInfo.bizStatus = res.bizStatus;
				return res;
			});
		},
		getCharge : function(orderId){
			return $.ajax({type: "POST",dataType:"json",
				url: contextPath + "rest/bikegw/charge/getcharge",
				data : {
					channel : "wx_pub",
					orderId : orderId
				}
			})
		},
		checkOrder : function(orderId){
			return $.ajax({type: "POST",dataType:"json",
				url: contextPath + "rest/bikegw/charge/checkOrderStatus",
				data : {
					orderId : orderId
				}
			})
		},
		scanQRCode : function(){
			var d = $.Deferred();
			weixin.scanQRCode()
			.done(function(res){
				var QRCode = res.resultStr;
				if(QRCode == null || QRCode.length !=11){
					d.reject();
					return;
				}
				var cityId = parseInt(QRCode.substring(0,4),36);
				var siteNum = parseInt(QRCode.substring(4,8),36);
				var parkNum = parseInt(QRCode.substring(8,10),36);
				if(cityId == null || siteNum == null || parkNum == null){
					d.reject();
					return;
				}
				var crc = parseInt(QRCode.substring(10),36);
				var crcSource = cityId+""+siteNum+""+parkNum;
				var crcResult = 0;
				for(var i =0;i < crcSource.length;i++){
					crcResult ^= crcSource.charAt(i);
				}
				//
				if(crcResult != crc){
					d.reject();
					return;
				}
				
				d.resolve({
					cityId : cityId,
					siteNum : siteNum,
					parkNum : parkNum
				})
			})
			.fail(d.reject);
			return d;	
		}
		
	}
});
