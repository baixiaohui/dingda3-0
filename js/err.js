define(function(){
	return{
		//-----系统服务类-----
		SUCESS : {code:"0",desc:"ok"},
		ILLEGAL_ACCESS 	: {code:"101",desc:"非法访问"},
		SECURE_KEY_ERR 	: {code:"102",desc:"密钥错误"},
		USER_NOT_LOGIN 	: {code:"103",desc:"用户未登录"},
		PARAM_VER_ERR 	: {code:"104",desc:"参数校验错误"},
		INVALID_TOKEN 	: {code:"105",desc:"无效访问令牌"},
		SERVER_ERR 		: {code:"106",desc:"服务错误"},
		OBJ_NOT_EXIST	: {code:"107",desc:"对象不存在"},
		TOO_FREQUENT	: {code:"108",desc:"操作频繁"},
		OTHER_ERROR 	: {code:"109",desc:"其他错误"},
		
		//-----微信类-----
		WX_JSSDK_ERR 	: {code:"201",desc:"微信JS-SDK验证失败"},
		
		//-----虚拟卡类----
		CARD_NEW 				: {code:"1001",desc:"新建"},
		CARD_OPEN_PAYING		: {code:"1002",desc:"业务开通支付中"},
		CARD_VALID 				: {code:"1003",desc:"有效"},
		CARD_RENT 				: {code:"1004",desc:"已租车"},
		CARD_OVERTIME_PAYING 	: {code:"1005",desc:"租车超时支付中"},	
		CARD_LOGOFF 			: {code:"1006",desc:"已注销"},
		CARD_LOST				: {code:"1007",desc:"已挂失"},
		CARD_LOCK				: {code:"1008",desc:"账户锁定"},
		CARD_NOT_EXIST			: {code:"1009",desc:"用户不存在"},
		
		//-----支付类----
		ORDER_NOT_EXIST			: {code:"1010",desc:"订单不存在"},
		ORDER_IS_NEW			: {code:"1011",desc:"订单已创建"},
		ORDER_IS_COMMIT			: {code:"1012",desc:"订单已提交"},
		ORDER_IS_FINISHED		: {code:"1013",desc:"订单已完成"},
		ORDER_IS_CANCELED		: {code:"1014",desc:"订单已取消"},
		ORDER_CRT_CHARGE_ERR	: {code:"1015",desc:"创建支付凭证失败"},
		
		//-----租车业务类----
		OPER_NOBIKE 		: {code:"2002",desc:"车位无车"},		
		OPER_UNLOCK_ERR		: {code:"2003",desc:"开锁操作失败"},
		OPER_OFFLINE		: {code:"2004",desc:"网点不在线"},
		OPER_CITY_ERR		: {code:"2005",desc:"城市代码不匹配"},
		OPER_PARKNUM_ERR	: {code:"2006",desc:"车位不存在"},
		OPER_SITENUM_ERR	: {code:"2007",desc:"网点不存在"},
		OPER_PARK_ERR		: {code:"2008",desc:"请尝试其他车位"},
		OPER_OUT_TIME 		: {code:"2009",desc:"非服务时间"},
		//-----充电业务类----
		CHARGE_USEING     	: {code:"3002",desc:"正在充电中"},
		CHARGE_FAIL     	: {code:"3009",desc:"充电失败"},
		findDescByCode : function(code){
			for(var p in this){
				if(this[p].code && this[p].code == code){
					return this[p].desc;
				}
			}
			return "抱歉,服务不可用请稍后再试...";
		},
		isOk : function(code){
			return code == this.SUCESS.code;
		}
	}
});