// ====程序入口====
var contextPath,appInfo;
(function getContextRoot() {
	var dataImport; // 页面入口模块
	var scripts = document.getElementsByTagName('script'), script, match;
	// 计算访问路径
	for (i = 0, ln = scripts.length; i < ln; i++) {
		script = scripts[i];
		scriptSrc = script.src;
		if (scriptSrc && scriptSrc.indexOf("require") != -1) {
			dataImport = script.attributes['data-import'].value;
			contextPath = scriptSrc.substring(0, scriptSrc.indexOf("js/require"));
			break;
		}
	}
	// eap模块路径
	var paths = {
		'jquery' : 'http://apps.bdimg.com/libs/jquery/2.1.4/jquery.min',
		'jweixin' : 'http://res.wx.qq.com/open/js/jweixin-1.0.0',
		'mustache' : 'js/mustache',
		'cookie' : 'js/cookie',
		'pingpp' : 'js/pingpp',
		'utils' : 'js/utils',
		'gps' : 'js/gps',
		'md5' : 'js/md5',
		'eap' : 'js/eap',
		'err' : 'js/err',
		'weixin' : 'js/weixin',
		'dajia' : 'js/dajia',
		'bike' : 'js/bike',
		'charge' : 'js/charge'
	}
	require.config({
		baseUrl : contextPath,
		paths : paths,
		shim : {}
	});
	// 入口程序
	require(["cookie","eap"],function(cookie,eap){
		//处理appInfo
		appInfo = eap.urlDecode(window.location.search.substring(1));
		if(!eap.isEmpty(appInfo.appId)){
			cookie.set("appId",appInfo.appId);
			cookie.set("frontType",appInfo.frontType);
		} else {
			appInfo.appId = cookie.get("appId");
			appInfo.frontType = cookie.get("frontType");
		}
		require(dataImport.split(","))
	});
})();
