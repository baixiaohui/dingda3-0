define(['eap','jquery','jweixin',"pingpp",'err',"utils"],function(eap,$,wx,pingpp,err,utils){
	//init 
	var ready = false;
	function initWeixinAPI(){
		var d = $.Deferred();
		if(ready){
			d.resolve(wx);
			return d;
		}
		console.info("init Weixin SDK...");
		$.ajax({
			type: "POST",dataType:"json",
			data : {
				checkUrl : location.href.split('#')[0]
			},
			url: contextPath + "rest/wechat/jssdktiket"
		})
		.done(function(data){
			if(!eap.isEmpty(data.appId)){
				wx.config({
						beta:true, 
				      debug: true, 
				      appId: data.appId,
				      timestamp: data.timestamp,
				      nonceStr: data.nonceStr,
				      signature: data.signature,
				      jsApiList: ['scanQRCode','openLocation','getLocation']
				});
				
				wx.ready(function(){
					ready = true;
					d.resolve(wx); 
					// 初始化设备库函数
					wx.invoke('openWXDeviceLib', {'brandUserName':'gh_199d1b4bd281'}, function(res){
        // alert(res.err_msg);
  				   });
				});
				
				wx.error(function(m){
					ready = false;
					d.reject(err.WX_JSSDK_ERR); 
				});
				
			} else {
				ready = false;
				d.reject(err.WX_JSSDK_ERR);
			}
		})
		.fail(d.reject);
		return d;
	}
	
//	initWeixinAPI();
	
	return {
		//二维码扫码
		scanQRCode : function(){
			var d = $.Deferred();
			initWeixinAPI()
			.done(function(wx){
				wx.scanQRCode({
				    needResult: 1,
				    scanType: ["qrCode","barCode"],
				    fail : d.reject,
					success : d.resolve
				});
			})
			.fail(d.reject);
			return d;
		},
		bluetoothSaomiao:function(){
			var d = $.Deferred();
			initWeixinAPI()
			.done(function(wx){
				wx.scanQRCode({
				    needResult: 1,
				    scanType: ["qrCode","barCode"],
				    fail : d.reject,
					success : d.resolve
				});

			})
			.fail(d.reject);
			return d;
		},
		//获取经纬度
		getLocation : function(){
			var d = $.Deferred();
			initWeixinAPI()
			.done(function(wx){
				wx.getLocation({
					type: 'wgs84', 
				    fail : d.reject,
					success : d.resolve
				});	
			})
			.fail(d.reject);
			return d;
		},
		wxpay : function(chargedata,callback){
			pingpp.createPayment(chargedata,callback);
		}
	}
});