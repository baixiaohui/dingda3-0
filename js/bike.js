define(['jquery','eap',appInfo.frontType,"gps","utils","md5","err"],function($,eap,operAPI,gps,utils,md5,err){
	var currentBizInfo = {};
	return {
		login : function(userName,passWord){
			return $.ajax({type: "POST",dataType:"json",
				url: contextPath + "rest/bikemt/security/clientlogin",
				data : {
					userName : userName,
					passWord : md5.calcMD5(passWord)
				}
			});
		},
		logout : function(userName,passwd){
			return $.ajax({type: "POST",dataType:"json",
				url: contextPath + "rest/bikemt/security/clientlogout"
			});
		},
		register : function(phoneNumber,password,vcode){
			return $.ajax({type: "POST",dataType:"json",
				url: contextPath + "rest/bikemt/member/register",
				data : {
					phoneNumber : phoneNumber,
					password : md5.calcMD5(password),
					vcode : vcode
				}
			});
		},
		updatepwd : function(oldPassword,newPassword){
			return $.ajax({type: "POST",dataType:"json",
				url: contextPath + "rest/bikemt/member/updatepwd",
				data : {
					oldPassword : md5.calcMD5(oldPassword),
					newPassword : md5.calcMD5(newPassword)
				}
			});
		},
		resetpwd : function(phoneNumber,newPassword,vcode){
			return $.ajax({type: "POST",dataType:"json",
				url: contextPath + "rest/bikemt/member/resetpwd",
				data : {
					phoneNumber : phoneNumber,
					newPassword : md5.calcMD5(newPassword),
					vcode : vcode
				}
			});
		},
		modifyPhonenum : function(phoneNumber,vcode){
			return $.ajax({type: "POST",dataType:"json",
				url: contextPath + "rest/bikemt/member/bindnewphone",
				data : {
					phoneNumber : phoneNumber,
					vcode : vcode
				}
			});
		},
		getResetPWDVCode : function(phoneNumber){
			return $.ajax({type: "POST",dataType:"json",
				url: contextPath + "rest/bikemt/security/getresetpwdvcode",
				data : {
					phoneNumber : phoneNumber
				}
			});
		},
		getRegistVCode : function(phoneNumber){
			return $.ajax({type: "POST",dataType:"json",
				url: contextPath + "rest/bikemt/security/getregistvcode",
				data : {
					phoneNumber : phoneNumber
				}
			});
		},
		listMails : function(){
			return $.ajax({type: "POST",dataType:"json",
				url: contextPath + "rest/bikemt/manager/listMail"
			});
		},
		submitMail : function(title,content){
			return $.ajax({type: "POST",dataType:"json",
				url: contextPath + "rest/bikemt/manager/submitMail",
				data : {
					title : title,
					content : content
				}
			});
		},
		//
		rentBike : function(siteNum,parkNum){
			return $.ajax({type: "POST",dataType:"json",
				url: contextPath + "rest/bikegw/bike/request",
				data : {
					siteNum : siteNum,
					parkNum : parkNum
				}
			})
			.then(function(res){
				currentBizInfo.bizStatus = res.bizStatus;
				return res;
			});
		},
		getCurrentBizInfo : function(){return currentBizInfo;},
		getBizInfo : function(){
			return $.ajax({type: "POST",dataType:"json",
				url: contextPath + "rest/bikegw/business/bizinfo"
			})
			.then(function(res){
				currentBizInfo = res;
				currentBizInfo.bizstatus = currentBizInfo.bizStatus;
				delete currentBizInfo.bizStatus;
				return res;
			});
		},
		queryOrders : function(ordertype){
			return $.ajax({type: "POST",dataType:"json",
				url: contextPath + "rest/bikegw/payment/getorders",
				data : {
					ordertype : ordertype || ""
				}
			})
		},
		queryRecords : function(retcount){
			return $.ajax({type: "POST",dataType:"json",
				url: contextPath + "rest/bikegw/business/records",
				data : {
					retcount : retcount || "10"
				}
			})
		},
		disableTrade : function(){
			return $.ajax({type: "POST",dataType:"json",
				url: contextPath + "rest/bikegw/business/disabletrade"
			})
			.then(function(res){
				currentBizInfo.bizStatus = res.bizStatus;
				return res;
			});
		},
		enableTrade : function(){
			return $.ajax({type: "POST",dataType:"json",
				url: contextPath + "rest/bikegw/business/enabletrade"
			})
			.then(function(res){
				currentBizInfo.bizStatus = res.bizStatus;
				return res;
			});
		},
		getCharge : function(orderId){
			return $.ajax({type: "POST",dataType:"json",
				url: contextPath + "rest/bikegw/payment/getcharge",
				data : {
					channel : appInfo.frontType == "dajia" ? "wx" : "wx_pub",
					orderId : orderId
				}
			})
		},
		checkOrder : function(orderId){
			return $.ajax({type: "POST",dataType:"json",
				url: contextPath + "rest/bikegw/payment/checkOrderStatus",
				data : {
					orderId : orderId
				}
			})
		},
		createPayment : function(chargedata,callback){
			operAPI.wxpay(chargedata,callback);
		},
		scanQRCode : function(){
			var d = $.Deferred();
			operAPI.scanQRCode()
			.done(function(res){
				var QRCode = res.resultStr;
				if(QRCode == null){
					d.reject();
					return;
				}
				var cityId,stationId,parkingId;
				if(QRCode.length != 11){//新二维码
					var codeInfo = eap.urlDecode(QRCode.substr(QRCode.indexOf("?") + 1));
					if(eap.isEmpty(codeInfo.citycode) || eap.isEmpty(codeInfo.sn) || codeInfo.sn.length < 7){
						d.reject();
						return;
					}
					cityId = parseInt(codeInfo.citycode);
					stationId = parseInt(codeInfo.sn.substr(0,codeInfo.sn.length - 3));
					parkingId = parseInt(codeInfo.sn.substr(codeInfo.sn.length - 3));
				} else {
					cityId = parseInt(QRCode.substring(0,4),36);
					stationId = parseInt(QRCode.substring(4,8),36);
					parkingId = parseInt(QRCode.substring(8,10),36);
					if(cityId == null || stationId == null || parkingId == null){
						d.reject();
						return;
					}
					var crc = parseInt(QRCode.substring(10),36);
					var crcSource = cityId+""+stationId+""+parkingId;
					var crcResult = 0;
					for(var i =0;i < crcSource.length;i++){
						crcResult ^= crcSource.charAt(i);
					}
					//
					if(crcResult != crc){
						d.reject();
						return;
					}
				}
				
				d.resolve({
					cityId : cityId,
					stationId : stationId,
					parkingId : parkingId
				})
			})
			.fail(d.reject);
			return d;	
		},
		getSiteInfo:function(stationId){
			return $.ajax({type: "POST",dataType:"json",
				url: contextPath + "rest/bikegw/netpoints/status",
				data : {
					stationId : stationId
				}
			})
		},
		
		getPointInfo:function(stationId){
			return $.ajax({type: "POST",dataType:"json",
				url: contextPath + "rest/bikegw/netpoints/posinfo",
				data : {
					stationId : stationId
				}
			})
		},
		getLocation : function(){
			var d = $.Deferred();
			var locationType = (appInfo.frontType == "dajia" ? "amap" : "gps");
			operAPI.getLocation()
			.done(function(res){
				d.resolve(gps.gpsToBaiduPoint(locationType ,parseFloat(res.latitude),parseFloat(res.longitude)))
			})
			.fail(d.reject);
			return d;
		},
		queryNearSites : function(centerPoint_,toPoint_){
			var d = $.Deferred();
			var map = new BMap.Map();
			var locationType = (appInfo.frontType == "dajia" ? "amap" : "gps");
			operAPI.getLocation()
			.done(function(res){
				var centerPoint = centerPoint_ || gps.gpsToBaiduPoint(locationType ,parseFloat(res.latitude),parseFloat(res.longitude));
				var toPoint = toPoint_ || centerPoint;
				$.ajax({type: "POST",dataType:"json",
					url: contextPath + "rest/bikegw/netpoints/nearstationstatus",
					data : {
						lon : centerPoint.lon,
						lat : centerPoint.lat,
						len : 3000
					},
					success : function(res){
						eap.each(res.data,function(data){
							data.distance_ = map.getDistance(new BMap.Point(toPoint.lon,toPoint.lat), new BMap.Point(data.lon,data.lat));
							data.distance = utils.formatDistance(data.distance_);
						});
						
						res.data.sort(function(a,b){
							return a.distance_ - b.distance_;
						});
						
						d.resolve(res);
					},
					error : d.reject
				});
			})
			.fail(d.reject);
			return d;
		}
	}
});
