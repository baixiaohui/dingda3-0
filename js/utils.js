define(['jquery',"eap"], function($,eap,gps) {
	// jqueryMobile 工具类
	var msgObject = $(".tips-prompt");
	var msgObject2 = $("#tipsNotice");
	
	var dialogObject = $("#tipsDialog");
	dialogObject.find("#tipsDialogOk").click(function() {
		dialogObject.css("display", "none");
		dialogObject.callback(true);
	});
	dialogObject.find("#tipsDialogCancel").click(function() {
		dialogObject.css("display", "none");
		dialogObject.callback(false);
	});
	
	return {
		
		formatDistance : function(distance){
			return distance > 1000 ? (distance/1000).toFixed(1) +"公里" : parseInt(distance)+"米";
		},
		formatMoney : function(v){
			v = v/100;
			v = (Math.round((v-0)*100))/100;
            v = (v == Math.floor(v)) ? v + ".00" : ((v*10 == Math.floor(v*10)) ? v + "0" : v);
            v = String(v);
            var ps = v.split('.'),
                whole = ps[0],
                sub = ps[1] ? '.'+ ps[1] : '.00',
                r = /(\d+)(\d{3})/;
            while (r.test(whole)) {
                whole = whole.replace(r, '$1' + ',' + '$2');
            }
            v = whole + sub;
            if (v.charAt(0) == '-') {
                return '-￥' + v.substr(1);
            }
            return "￥" +  v;
		},
		formatElapsedTime : function(elapsedTime){
			if(elapsedTime < 1000){
				return "0秒";
			}
			var day = parseInt(elapsedTime / 86400000);
			var hour = parseInt((elapsedTime % 86400000) / 3600000);
			var min = parseInt((elapsedTime % 86400000) % 3600000 / 60000);
			var ss = parseInt((elapsedTime % 86400000) % 3600000 % 60000 / 1000);
	
			var renttime = [];
			if (day != 0) {
				renttime.push(day,"天");
			}
			if (hour != 0) {
				renttime.push(hour,"小时");
			}
			if (min != 0) {
				renttime.push(min,"分");
			}
			if (ss != 0) {
				renttime.push(ss,"秒");
			}
			return renttime.join("");
		},
		formatElapsedDate : function(str1,str2){
			if(eap.isEmpty(str1) || eap.isEmpty(str2)){
				return "";
			}
		    var datatime1 = str1.split(' ');
			arraydata1 = datatime1[0].split('-');
			arraytime1 = datatime1[1].split(':')
	
			var datatime2 = str2.split(' ');
			arraydata2 = datatime2[0].split('-');
			arraytime2 = datatime2[1].split(':')
	
			var data1 = new Date(arraydata1[0], arraydata1[1], arraydata1[2],
					arraytime1[0], arraytime1[1], arraytime1[2]);
			var data2 = new Date(arraydata2[0], arraydata2[1], arraydata2[2],
					arraytime2[0], arraytime2[1], arraytime2[2]);
			return this.formatElapsedTime(data2 - data1);
		},
		setButtonState : function(button,disabled){
			button = $(button);
			if(disabled === true){
				button.removeClass("btn-primary").addClass("btn-disabled");
				button.attr("disabled",true);
			} else {
				button.removeClass("btn-disabled").addClass("btn-primary");
				button.attr("disabled",false);
			}
		},
		notice : function(msg, options){
			msgObject2.fadeIn(200);
			msgObject2.find("#tipsNoticeMsg").html(msg);
			setTimeout(function(){ 
				msgObject2.fadeOut(200);
			},2000);
		},
		msg : function(msg, options) {
			msgObject.fadeIn(200);
			msgObject.find(".tips-prompt-msg").html(msg);
			setTimeout(function(){ 
				msgObject.fadeOut(200);
			},2000);
		},
		hideLoading : function(){
			$("#LoadingMask").hide();
		},
		dialog : function(options, callback) {
			var msg = options.msg || ""
			var title = options.title || "系统提示"
			dialogObject.css("display", "block");
			dialogObject.find("#tipsDialogMsg").html(msg);
			dialogObject.find("#tipsDialogTitle").html(title);
			if(options.cancelTxt){
				dialogObject.find("#tipsDialogCancel").html(options.cancelTxt);
			}
			if(options.okTxt){
				dialogObject.find("#tipsDialogOk").html(options.okTxt);
			}
			dialogObject.callback = callback || eap.emptyFn;
		}
	}
});