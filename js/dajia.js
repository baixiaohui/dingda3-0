define(['eap','jquery','err'],function(eap,$,err){
	da.djJsNeedInterface = true;
	da.djJsReady = true;
	da.djJsDebug = false;
	da.readyFun;
	da.errorFun;
	da.functionMap= {};
	da.version;
	da.djInjs = false;
	da.check = function(data){
		this.djInjs = true;
		if(data=="ok"){
			this.djJsReady = true;
			if(this.readyFun)
				this.readyFun();
			return;
		}
		if(data=="error"){
			this.djJsReady = false;
			if(this.errorFun)
				this.errorFun();
			return;
		}
		if (typeof (data) != "object") {
			data = eval('(' + data + ')');
		}
		if(data.msg=="ok"){
			this.djJsReady = true;
			this.functionMap = data.fun||{};
			if(this.readyFun)
				this.readyFun();
		}else{
			this.djJsReady = false;
			if(this.errorFun)
				this.errorFun();
		}
	};
	
	da.canTrans = function(){
		return this.djJsReady && typeof(djInternal)!='undefined';
	};


	//对前台页面提供的接口
  	da.config = function(v){
		this.djJsDebug=v.debug;
		this.djJsNeedInterface = true;
	};
	da.ready = function(v){
		this.readyFun = v;
		if(this.djInjs && da.djJsReady){
			this.readyFun();
		}
	};
	da.error = function(v){
		this.errorFun = v;
		if(this.djInjs && da.djJsReady == false){
			this.errorFun();
		}
	};
	da.checkFun = function(v){
		if(this.functionMap && eval('this.functionMap.'+v.funName)){
			v.success();
		}else if(this.canTrans()){
			djInternal.callHandler('checkFun', {'funName':v.funName},
			function (callback){
			  switch(callback.code){
				case 'success':
				  v.success();
				break;
				case 'fail':
				  v.fail();
				break;
				default:
				  v.fail();
				break;
			  }
			});
		}else{
			v.fail();
		}
	};
	da.showLocation = function(v){
		if(this.canTrans())
			djInternal.callHandler('showLocation', {'lat':v.lat,'lon':v.lon,'name':v.name,'addr':v.addr});
	};
	da.getLocation = function(v){
		if(this.canTrans())
			djInternal.callHandler('getLocation', {'current':v.current}, 
			function (callback){
			  switch(callback.code){
				case 'success':
				  v.success(callback.res);
				break;
				case 'fail':
				  v.fail(callback.res);
				break;
				default:
				  v.fail(callback.res);
				break;
			  }
			});
	};
	da.showScan = function(v){
		if(this.canTrans())
			djInternal.callHandler('showScan', {'needResult':v.needResult}, 
			function (callback){
			  switch(callback.code){
				case 'success':
				  v.success(callback.res);
				break;
				case 'fail':
				  v.fail(callback.res);
				break;
				default:
				  v.fail(callback.res);
				break;
			  }
			});
	};
	da.showQRCode = function(v){
		if(this.canTrans())
			djInternal.callHandler('showQRCode', {'url':v.url}, 
			function (callback){
			  switch(callback.code){
				case 'success':
				  v.success(callback.res);
				break;
				case 'fail':
				  v.fail(callback.res);
				break;
				default:
				  v.fail(callback.res);
				break;
			  }
			});
	};
	da.alipay = function(v){
		if(this.canTrans())
			djInternal.callHandler('alipay', {'content':v.content}, 
			function (callback){
			  switch(callback.code){
				case 'success':
				  v.success(callback.res);
				break;
				case 'fail':
				  v.fail(callback.res);
				break;
				default:
				  v.fail(callback.res);
				break;
			  }
			});
	};
	da.wxpay = function(v){
		if(this.canTrans())
			djInternal.callHandler('wxpay', {'appid':v.appid,'noncestr':v.noncestr,'packageStr':v.packageStr,'partnerid':v.partnerid,'prepayid':v.prepayid,'timestamp':v.timestamp,'sign':v.sign}, 
			function (callback){
			  switch(callback.code){
				case 'success':
				  v.success(callback.res);
				break;
				case 'fail':
				  v.fail(callback.res);
				break;
				default:
				  v.fail(callback.res);
				break;
			  }
			});
	};
	da.createWindow = function(v){
		if(this.canTrans())
			djInternal.callHandler('createWindow',{'url':v.url});
	};
	return {
		//二维码扫码
		scanQRCode : function(){
			var d = $.Deferred();
			da.showScan({
			    needResult: 1,
			    fail : d.reject,
				success :  function(res){
			    	d.resolve({
			    		resultStr : res
			    	});
			    }
			});
			return d;
		},
		getLocation : function(){
			var d = $.Deferred();
			da.getLocation({
				current : 1,
			    fail : d.reject,
				success :  function(res){
			    	d.resolve({
			    		latitude : res.lat,
			    		longitude : res.lon
			    	});
			    }
			});	
			return d;
		},
		wxpay : function(chargedata,callback){
			var chargeData =  $.parseJSON(chargedata);
			var wxData = chargeData.credential.wx;
			da.wxpay({
				appid : wxData.appId,			//微信移动支付appID
				noncestr : wxData.nonceStr,		//随机字符串
				packageStr : 'Sign=WXPay',		//扩展字段:固定值Sign=WXPay
				partnerid : wxData.partnerId,	//商户号
				prepayid : wxData.prepayId,		//预支付交易会话ID
				timestamp : wxData.timeStamp,	//时间戳
				sign : wxData.sign,				//签名
				success:function(res){
					alert("success:"+res);
				},
				fail:function(res){
					alert("fail:"+res);
				}
			})
		}
	};
});